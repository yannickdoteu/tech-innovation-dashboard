# Welcome to Volt Europa's Tech Innovation Dashboard

Everything related to the Tech Innovation team happens here or starts here. See this as our shared hard drive with all the information needed for transparent communication and efficient collaboration.

## Missing something? Or want to improve?
You can make a suggestion for anything in this dashboard by making a pull request via git. You can find more information in the [Contributing.md](Contributing.md) file.

# About EUR Tech Innovation

## How we use technlogy will define the success of Volt
Technology is everywhere in our society, and it is especially present in Volt. 
And that makes sense. Because of our modern and future-orientated nature, Volt is a product of our time. Volt is a product of the digital age we live in.
It is not hard to imagine that without the internet, Volt would not have existed. This fact alone has a big impact on who we are and how we are organized.
To show you that, let's break it down a little. We are people all over Europe and together we work on our idea of what Europe is and could be. We are able to do this because we work together on our computers. If you look at it like this, then Volt is essentially just people with computers.
Now, if we understand what happens in between our computers and if we understand how this impacts us and how it impacts our organization, then we have the superpower that we can use to our advantage.

## We bring Volt's inhouse technical expertise to the parts in Volt where it is most needed and effective.
In contrast to the vast majority of other political parties, we have actual mature and well-though policies on how we think politics should handle the digital revolution. Because of this, Volt has attracted a lot of tech-savvy people! We are in the fortunate position that we have a lot of expertise in our party, so let's use it! To make the best use of the time and energy of these experts, the Tech Innovation teams wants to connect those Volters that run into (sytematic) problems with these experts. In other words: we want to smartly bring together supply and demand for technical solutions!


## We do this by on one hand getting insight in the needs of Volt chapters and teams and on the other hand providing resources and guidance to projects that aim to find evidence for smart technical solutions to these needs.

**That boils down to these actions:**
- Being available for and reaching out to chapters and teams in Volt to help them identify and signal their technical needs
- Keeping track of projects in Evert part of Volt involving technical innovation
- Strategically supporting projects from any group of Volters that aim to find technical solutions to the needs
- Informing our organization about the activities relating Tech Innovation
- [TBD: adding more bullets and subbullets providing more explanation]

The Tech Innovation Team (previously: Experimental Software Group or 'ESG') is a subteam of Volt Europa's Tech team.

``` mermaid
graph BT
    A[Tech Innovation] --> B[EUR Tech] --> C[Volt Europa]
    X[Project X] --> A
    Y[Project Y] --> A
    Z[Project Z] --> A
```

# How this dashboard helps us
TBD