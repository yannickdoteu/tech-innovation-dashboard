# "Projects/On our radar" folder

In this folder you can find the information about all ongoing projects, as well as legacy (old/archived) projects under ESG and other tech-related projects not under the supervision of the Tech Innovation team.
