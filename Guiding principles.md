# Guiding principes

- In line with policies and values
- Resource heaviness
    - Money
    - Time
- Strategic objectives
- Measurability
- Involvement with other parts (teams, chapters) of Volt
- Support from multiple Volters from different work areas