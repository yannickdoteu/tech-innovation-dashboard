# Project lifecycle

# Stage 1: 💡idea conceptualization

## CONTEXT:

- What is the background of your idea?
    - When did it come up? What was the situation?
    - Why is this important to you personally?
    - What people where involved in the making of this idea?

## DEFINITION:

- What is the idea exactly?
    - What is it about and what not?
    - What components make up this idea?
    - What problem is being addressed?
        - Why is this problem real?
    - How would it look like if your idea was perfectly implemented?
        - What parts of Volt and what business processes are involved or affected by this idea?

## THEORY:

- Why do you think this is a good idea?
    - What are the benefits and who would benefit from it?
    - What are the risks?
    - What are the reasons for the idea?
    - How does it help Volt achieve our strategic goals and realize our agenda?

# Stage 2: defining proof of concept

- Which components of the idea can be proven with an experiment?
- How should the prove of each of these components look like?
    - How is it quantified and measured?
    - What values for these things should minimally be reached for the component to be proven?
    - What environment is needed
- Under what conditions can the concept be proven?
    - Environmental conditions
        - What budget do you need?
        - What people need to be committed?
    - How much time is needed?

At the end of this stage — following our guiding principles — the Tech Innovation team will decide whether or not we can devote our time and resources into realizing the proof of concept of your idea.

# Stage 3: Realizing proof of concept

- Realizing technical conditions like infrastructure for experiment
- Ensuring that the conditions are met to ensure a healthy experiment
    - Allocating budget
    - Communication with stakeholders and participants
    - Technical conditions
        - Infrastructure for experiment
- Ensuring measuring of those things that need to prove the concept

# Stage 4: evaluating proof of concept

- Is the proof of concept sufficient to scale up the idea?
    - What are the differences to the idea in theory and the idea in practice?
    - Involving stakeholders
- How was the process thus far? What could be learned at this point that can be applied to ongoing or future projects?

# Stage 5: Adoption in the organization

- Dividing responsibilities
- Change management
    - Communication with stakeholders
- Ongoing evaluation
    - Does a new project related to this idea need to be started need to be started?
